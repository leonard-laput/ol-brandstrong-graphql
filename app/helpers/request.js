const appRoot = require('app-root-path');
const axios = require('axios');

const config = require(`${appRoot}/config`);

module.exports = {
	get(url, query = {}, token = '') {
		return new Promise((resolve, reject) => {
			axios({
				method: 'GET',
				url: `${config.api}${url}`,
				headers: {
					Accept: 'application/json',
					'Content-Type': 'application/json',
					Authorization: `Bearer ${token}`
				},
				params: query
			})
				.then(response => resolve(response.data))
				.catch(error => reject(error));
		});
	},
	post(url, body = {}, token = '') {
		return new Promise((resolve, reject) => {
			axios({
				method: 'POST',
				url: `${config.api}${url}`,
				headers: {
					Accept: 'application/json',
					'Content-Type': 'application/json',
					Authorization: `Bearer ${token}`
				},
				data: body
			})
				.then(response => resolve(response.data))
				.catch(error => reject(error));
		});
	},
	patch(url, body = {}, token = '') {
		return new Promise((resolve, reject) => {
			axios({
				method: 'PATCH',
				url: `${config.api}${url}`,
				headers: {
					Accept: 'application/json',
					'Content-Type': 'application/json',
					Authorization: `Bearer ${token}`
				},
				data: body
			})
				.then(response => resolve(response.data))
				.catch(error => reject(error));
		});
	},
	delete(url, token = '') {
		return new Promise((resolve, reject) => {
			axios({
				method: 'DELETE',
				url: `${config.api}${url}`,
				headers: {
					Accept: 'application/json',
					'Content-Type': 'application/json',
					Authorization: `Bearer ${token}`
				}
			})
				.then(response => resolve(response.data))
				.catch(error => reject(error));
		});
	}
};
