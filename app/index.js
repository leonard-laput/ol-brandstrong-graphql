const express = require('express');
const graphqlHTTP = require('express-graphql');
const cors = require('cors');

const definitions = require('./schemas/definitions');
const resolvers = require('./schemas/resolvers');

const app = express();

// Allow cross-origin
app.use(cors());

app.use(
	'/',
	graphqlHTTP({
		schema: definitions,
		rootValue: resolvers,
		graphiql: true
	})
);

const port = process.env.PORT || 3001;
app.listen(port, () => console.log('Server started at port:', port)); // eslint-disable-line
