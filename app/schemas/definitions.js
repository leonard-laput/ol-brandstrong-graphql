const { buildSchema } = require('graphql');

module.exports = buildSchema(`
	type Query {
		medias: MediasResult!

		media(id: ID!): MediaResult

		user(
			id: ID!
			token: String!
		): UserResult

		comments(
			media: ID!
		): CommentsResult

		login(
			username: String!
			password: String!
		): LoginResult

		logout(
			token: String!
		): LogoutResult

		validateUserToken(
			token: String!
		): ValidateUserTokenResult
	}

	type Mutation {
		addMedia(
			file_name: String!
			file: String!
			is_media: Boolean
			is_avatar: Boolean
			token: String!
		): MediaResult!

		addComment(
			text: String!
			asset: ID!
			token: String!
		): CommentResult!

		updateComment(
			id: ID!
			text: String!
			token: String!
		): CommentResult!

		updateUser(
			id: ID!
			email: String
			username: String
			password: String
			name: String
			country: String
			state: String
			city: String
			avatar_url: String
			token: String!
		): UserResult!

		signup(
			email: String!
			username: String!
			password: String!
			name: String!
			country: String
			state: String
			city: String
		): SignupResult
	}

	type Meta {
		code: Int!
		message: String!
	}

	type Media {
		_id: ID
		url: String
		user: User
	}

	type MediasResult {
		meta: Meta!
		data: [Media]!
	}

	type MediaResult {
		meta: Meta!
		data: Media
	}

	type User {
		_id: ID
		email: String
		username: String
		name: String
		country: String
		state: String
		city: String
		avatar_url: String
	}

	type UserResult {
		meta: Meta!
		data: User
	}

	type Comment {
		_id: ID
		text: String
		user: User,
		created_at: String
	}

	type CommentsResult {
		meta: Meta!
		data: [Comment]!
	}

	type CommentResult {
		meta: Meta!
		data: Comment
	}

	type Auth {
		token: String!
	}

	type Login {
		auth: Auth
		user: User
	}

	type LoginResult {
		meta: Meta!
		data: Login!
	}

	type LogoutResult {
		meta: Meta!
	}

	type SignupResult {
		meta: Meta!
	}

	type ValidateUserTokenResult {
		meta: Meta!
	}
`);
