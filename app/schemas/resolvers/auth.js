const appRoot = require('app-root-path');

const request = require(`${appRoot}/app/helpers/request`);

module.exports = {
	login: async args => {
		const { username, password } = args;
		const response = await request.post('/v1/auth/login', { username, password });
		const { meta, data } = response;
		return {
			meta,
			data: meta.code !== 200 ? {} : data
		};
	},

	logout: async args => {
		const { token } = args;
		const response = await request.post('/v1/auth/logout', { token });
		const { meta } = response;
		return {
			meta
		};
	},

	validateUserToken: async args => {
		const { token } = args;
		const response = await request.post('/v1/auth/validate-user-token', { token });
		const { meta } = response;
		return {
			meta
		};
	},

	signup: async args => {
		const { email, username, password, name, country, state, city } = args;
		const signup = {
			email,
			username,
			password,
			name,
			...(country && { country }),
			...(state && { state }),
			...(city && { city })
		};
		const response = await request.post('/v1/auth/signup', signup);
		const { meta } = response;
		return {
			meta
		};
	}
};
