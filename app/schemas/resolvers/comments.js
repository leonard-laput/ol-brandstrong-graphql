const appRoot = require('app-root-path');

const request = require(`${appRoot}/app/helpers/request`);

module.exports = {
	comments: async args => {
		const { media, token } = args;
		const response = await request.get('/v1/comments', { asset: media }, token);
		const { meta, data } = response;
		return {
			meta,
			data: meta.code !== 200 ? [] : data.comments
		};
	},

	addComment: async args => {
		const { text, asset, token } = args;
		const comment = {
			text,
			asset
		};
		const response = await request.post('/v1/comments', { comment }, token);
		const { meta, data } = response;
		return {
			meta,
			data: meta.code !== 200 ? {} : data.comment
		};
	},

	updateComment: async args => {
		const { id, text, token } = args;
		const comment = {
			text
		};
		const response = await request.patch(`/v1/comments/${id}`, { comment }, token);
		const { meta, data } = response;
		return {
			meta,
			data: meta.code !== 200 ? {} : data.comment
		};
	}
};
