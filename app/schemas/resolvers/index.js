const auth = require('./auth');
const comments = require('./comments');
const medias = require('./medias');
const users = require('./users');

module.exports = {
	...auth,
	...comments,
	...medias,
	...users
};
