const appRoot = require('app-root-path');

const request = require(`${appRoot}/app/helpers/request`);

module.exports = {
	medias: async () => {
		const response = await request.get('/v1/assets', { is_media: true });
		const { meta, data } = response;
		return {
			meta,
			data: meta.code !== 200 ? [] : data.assets
		};
	},

	media: async args => {
		const { id } = args;
		const response = await request.get(`/v1/assets/${id}`);
		const { meta, data } = response;
		return {
			meta,
			data: meta.code !== 200 ? {} : data.asset
		};
	},

	addMedia: async args => {
		const { file_name, file, is_media = false, is_avatar = false, token } = args;
		const asset = {
			file_name,
			file,
			is_media,
			is_avatar
		};
		const response = await request.post('/v1/assets', { asset }, token);
		const { meta, data } = response;
		return {
			meta,
			data: meta.code !== 200 ? {} : data.asset
		};
	}
};
