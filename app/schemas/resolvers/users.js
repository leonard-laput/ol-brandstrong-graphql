const appRoot = require('app-root-path');

const request = require(`${appRoot}/app/helpers/request`);

module.exports = {
	user: async args => {
		const { id, token } = args;
		const response = await request.get(`/v1/users/${id}`, {}, token);
		const { meta, data } = response;
		return {
			meta,
			data: meta.code !== 200 ? {} : data.user
		};
	},

	updateUser: async args => {
		const { id, email, username, password, name, country, state, city, avatar_url, token } = args;
		const user = {
			...(email && { email }),
			...(username && { username }),
			...(password && { password }),
			...(name && { name }),
			...(country && { country }),
			...(state && { state }),
			...(city && { city }),
			...(avatar_url && { avatar_url })
		};
		const response = await request.patch(`/v1/users/${id}`, { user }, token);
		const { meta, data } = response;
		return {
			meta,
			data: meta.code !== 200 ? {} : data.user
		};
	}
};
